from sklearn.decomposition import PCA
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import *
import numpy.matlib
from numpy import linalg as LA

plt.style.use('classic')
rcParams['mathtext.fontset'] = 'custom'
rcParams['mathtext.it'] = u'Arial:italic'
rcParams['mathtext.rm'] = u'Arial'


class myPCA():
	def __init__(self,flap,training,validation):
		self.flap = flap
		self.training = training
		self.validation = validation

	################################
	# Principal Component Analysis #
	################################

	def doPCA(self):

		Y = self.training

		pca = PCA(0.999)
		pca.fit(Y)
		np.savetxt('PC_%s.txt'%self.flap,pca.components_,fmt='%.6f')
		
		print(np.cumsum(100*pca.explained_variance_ratio_))

		Y_projected_t = pca.transform(Y)
		np.savetxt('score_allstress_%sFlap_900T.txt'%self.flap, Y_projected_t, fmt='%.6f')

		##############
		# Validation #
		##############
		YY = self.validation
		Y_projected_v = pca.transform(YY)
		np.savetxt('score_allstress_%sFlap_300V.txt'%self.flap, Y_projected_v, fmt='%.6f')
		return pca, Y_projected_t, Y_projected_v
		
	########################################
	# Inverse Principal Component Analysis #
	########################################

	def doInvPCA(self,PC):

		ThePCA = PC

		Z = np.zeros((1500,ThePCA.explained_variance_ratio_.shape[0]))
		for i in range(ThePCA.explained_variance_ratio_.shape[0]):
			Z[:,i] = np.loadtxt('%sscore_%sFlap_300V.txt'%(i+1,self.flap))[:,0]

		DATA_new = ThePCA.inverse_transform(Z)
		
		np.savetxt('InvPCA_random_%sFlap_300V.txt'%self.flap, DATA_new, fmt='%f')
		return DATA_new
	
	def doInvPCA_probability(self,PC):
		
		ThePCA = PC

		Z = np.zeros((100000,ThePCA.explained_variance_ratio_.shape[0]))
		for i in range(ThePCA.explained_variance_ratio_.shape[0]):
			Z[:,i] = np.loadtxt('%sscore_%sFlap_ND_900T_y_posterior_all.txt'%(i+1,self.flap))[100,:]

		DATA_new = ThePCA.inverse_transform(Z)
		print(np.max(DATA_new))

		proba = np.zeros(DATA_new.shape[1])
		
		for ii in range(DATA_new.shape[1]):
			proba[ii]=(np.abs(DATA_new[:,ii])>0.2).sum()/100000.*100
		np.savetxt('InvPCA_%sFlap_ND_900T_y_30mm_prabability_larger_than_200kPa.txt'%self.flap, proba, fmt='%f')
		
		return DATA_new

	#################################
	# Cumulative Explained Variance #
	#################################

	def CEV(self):

		Y = self.training

		pca = PCA(15)
		pca.fit(Y)
	
		Y_cm = np.cumsum(100*pca.explained_variance_ratio_)
		x = np.linspace(1,Y_cm.shape[0],Y_cm.shape[0])
		X = np.array([x,Y_cm]).T
		np.savetxt('cmExplainedVar_allstress_%s_noPEAK.txt'%self.flap, X, fmt='%.6f')

	########################
	# l2-norm of the error #
	########################

	def l2norm_error(self,predicted_val):
		prediction = predicted_val*1000
		truth = self.validation

		error = LA.norm(truth-prediction,axis=1)/LA.norm(truth,axis=1)
	
		np.savetxt('l2norm_error_%sFlap_300V.txt'%self.flap,error,fmt='%.6f')
		print(np.mean(error),np.max(error))
		
		mu = np.mean(error)  # mean of distribution
		sigma = np.std(error)  # standard deviation of distribution
		#x = mu + sigma * np.random.randn(437)
		
		fig, ax = plt.subplots()
		num_bins = 60 # rotation
		n, bins, patches = ax.hist(error, num_bins, alpha = 0.25)

		y = mlab.normpdf(bins, mu, sigma)

		ax.set_xlabel(r'$\mathit{l_{2}} \mathrm{-norm}\ \mathrm{of}\ \mathrm{the}\ \mathrm{error}$',fontsize=26,fontname = "Times New Roman")
		ax.set_ylabel('Count',fontsize=26,fontname = "Times New Roman")
		plt.xticks(fontsize=23,fontname = "Times New Roman")
		plt.yticks(fontsize=23,fontname = "Times New Roman")
		ax.tick_params(axis='x', pad=15)
		ax.tick_params(axis='y', pad=15)
		x_axis = np.max(ax.get_xlim())-np.min(ax.get_xlim())
		y_axis = np.max(ax.get_ylim())-np.min(ax.get_ylim())
		ax.set_aspect(x_axis/y_axis)
		plt.tight_layout()
		plt.gcf().subplots_adjust(bottom=0.18,left=0.18)
		#plt.show()
		plt.savefig('l2norm_error_%sFlap_300V.pdf'%self.flap,format='pdf',dpi=1200)

	#########################
	# Standardized residual #
	#########################

	def std_residual(self,v_score,PC):
		ThePCA = PC
		truth = v_score
		std_res = np.zeros((1500,ThePCA.explained_variance_ratio_.shape[0]))

		for i in range(ThePCA.explained_variance_ratio_.shape[0]):
			
			Mp_tmp = np.loadtxt('%sscore_%sFlap_300V.txt'%(i+1,self.flap))[:,0]
			Up_tmp = np.loadtxt('%sscore_%sFlap_300V.txt'%(i+1,self.flap))[:,1]
			Sp_tmp = (Up_tmp-Mp_tmp)/1.96
			std_res[:,i] = (truth[:,i]-Mp_tmp)/Sp_tmp

			fig, ax = plt.subplots()
			x_idx = np.linspace(1,std_res.shape[0],std_res.shape[0])
			plt.scatter(x_idx,std_res[:,i])
			ax.set_xlabel('Sample index',fontsize=26,fontname = "Times New Roman")
			ax.set_ylabel('Standardized residual',fontsize=26,fontname = "Times New Roman")
			plt.xticks(fontsize=23,fontname = "Times New Roman")
			plt.yticks(fontsize=23,fontname = "Times New Roman")
			x_axis = np.max(ax.get_xlim())-np.min(ax.get_xlim())
			y_axis = np.max(ax.get_ylim())-np.min(ax.get_ylim())
			ax.set_aspect(x_axis/y_axis)
			ax.tick_params(axis='x', pad=15)
			ax.tick_params(axis='y', pad=15)
			plt.tight_layout()
			plt.gcf().subplots_adjust(bottom=0.18,left=0.18)
			plt.savefig('%sscore_%sFlap_std_res.pdf'%(i+1,self.flap), dpi=1200)

		np.savetxt('std_residual_%sFlap_300V.txt'%self.flap,std_res,fmt='%.6f')




