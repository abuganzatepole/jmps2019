import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import *
import GPy
import numpy.matlib
from numpy import linalg as LA

plt.style.use('classic')
rcParams['mathtext.fontset'] = 'custom'
rcParams['mathtext.it'] = u'Arial:italic'
rcParams['mathtext.rm'] = u'Arial'


class myGPR():
	def __init__(self,flap,input_t,score_t,input_v):
		self.flap = flap
		self.input_t = input_t
		self.score_t = score_t
		self.input_v = input_v

	def doGPR(self):	
		X = self.input_t # c10, k1, k2, size of scar
		X[:,0] *= 2 # make C10 to mu

		Xp_v = self.input_v # validation input
		Xp_v[:,0] *= 2 # make C10 to mu
		z = self.score_t

		for i in range(z.shape[1]):
			Z = z[:,i]
			if Z.ndim == 1:
				Z = Z[:, None]

			X_m = np.mean(X, axis=0)
			X_s = np.std(X, axis=0)
			X_scaled = (X - X_m) / X_s
			
			k = GPy.kern.RBF(X_scaled.shape[1], ARD=True)
			model = GPy.models.GPRegression(X_scaled, Z, k)
			
			model.optimize()
			print('*' * 80)
			print('Optimized model:')
			print(model)
			print('Lengthscales:')
			print(model.kern.lengthscale)
			predict = lambda Xp: model.predict_noiseless((Xp - X_m) / X_s)
			#predict = lambda Xp: model.predict((Xp - X_m) / X_s)
			Xt = np.linspace(40, 22.22, 200)[:,None]
			# young group
			mu = 5.866967
			k1 = 73.18767
			k2 = 73.21727
			Xp = np.hstack([np.matlib.repmat(mu, Xt.shape[0], 1),np.matlib.repmat(k1, Xt.shape[0], 1), np.matlib.repmat(k2, Xt.shape[0], 1), Xt])
			
			Mp, Vp = predict(Xp)
			Sp = np.sqrt(Vp)
			Lp = Mp - 1.96 * Sp
			Up = Mp + 1.96 * Sp

			fig, ax = plt.subplots()
			plt.plot(Xt,Mp,color='red',label='Predictive mean')
			ax.fill_between(Xt.flatten(), Lp.flatten(), Up.flatten(), alpha=0.25, color='grey', label='95% predictive intervals')
			plt.legend(loc='best')
			ax.set_xlabel('Diameter of scar (mm)',fontname = "Times New Roman",fontsize=26)
			ax.set_ylabel(r'$\mathit{y_{%i}}$'%(i+1),fontname = "Times New Roman",fontsize=26)
			plt.xticks(fontsize=23,fontname = "Times New Roman")
			plt.yticks(fontsize=23,fontname = "Times New Roman")
			plt.xlim([20,42])
			ax.tick_params(axis='x', pad=15)
			ax.tick_params(axis='y', pad=15)
			x_axis = np.max(ax.get_xlim())-np.min(ax.get_xlim())
			y_axis = np.max(ax.get_ylim())-np.min(ax.get_ylim())
			ax.set_aspect(x_axis/y_axis)
			plt.tight_layout()
			plt.gcf().subplots_adjust(bottom=0.18,left=0.18)
			plt.savefig('%iscore_%sFlap_y.pdf'%(i+1,self.flap), dpi=1200)

			# old group
			mu = 70.81967
			k1 = 8178.59
			k2 = 80.27857
			Xp = np.hstack([np.matlib.repmat(mu, Xt.shape[0], 1),np.matlib.repmat(k1, Xt.shape[0], 1), np.matlib.repmat(k2, Xt.shape[0], 1), Xt])
			
			Mp, Vp = predict(Xp)
			Sp = np.sqrt(Vp)
			Lp = Mp - 1.96 * Sp
			Up = Mp + 1.96 * Sp

			
			fig, ax = plt.subplots()
			plt.plot(Xt,Mp,color='red',label='Predictive mean')
			ax.fill_between(Xt.flatten(), Lp.flatten(), Up.flatten(), alpha=0.25, color='grey', label='95% predictive intervals')
			plt.legend(loc='best')
			ax.set_xlabel('Diameter of scar (mm)',fontname = "Times New Roman",fontsize=26)
			ax.set_ylabel(r'$\mathit{y_{%i}}$'%(i+1),fontname = "Times New Roman",fontsize=26)
			plt.xticks(fontsize=23,fontname = "Times New Roman")
			plt.yticks(fontsize=23,fontname = "Times New Roman")
			plt.xlim([20,42])
			ax.tick_params(axis='x', pad=15)
			ax.tick_params(axis='y', pad=15)
			x_axis = np.max(ax.get_xlim())-np.min(ax.get_xlim())
			y_axis = np.max(ax.get_ylim())-np.min(ax.get_ylim())
			ax.set_aspect(x_axis/y_axis)
			plt.tight_layout()
			plt.gcf().subplots_adjust(bottom=0.18,left=0.18)
			plt.savefig('%iscore_%sFlap_o.pdf'%(i+1,self.flap), dpi=1200)

			##############
			# Validation #
			##############

			predict = lambda Xp: model.predict((Xp - X_m) / X_s)
			Mp, Vp = predict(Xp_v)
			Sp = np.sqrt(Vp)
			Lp = Mp - 1.96 * Sp
			Up = Mp + 1.96 * Sp

			ZZ = np.zeros((Mp.shape[0],3),dtype=float)
			ZZ[:,0] = np.reshape(Mp,Mp.shape[0])
			ZZ[:,1] = np.reshape(Up,Up.shape[0])
			ZZ[:,2] = np.reshape(Lp,Lp.shape[0])
			np.savetxt('%iscore_%sFlap_300V.txt'%(i+1,self.flap),ZZ,fmt='%.6f')

	def GPR_w_error(self):

		X = self.input_t # c10, k1, k2, size of scar
		X[:,0] *= 2 # make C10 to mu

		Xp_v = self.input_v # validation input
		Xp_v[:,0] *= 2 # make C10 to mu
		z = self.score_t

		for i in range(z.shape[1]):
			Z = z[:,i]
			if Z.ndim == 1:
				Z = Z[:, None]

			X_m = np.mean(X, axis=0)
			X_s = np.std(X, axis=0)
			X_scaled = (X - X_m) / X_s
			
			k = GPy.kern.RBF(X_scaled.shape[1], ARD=True)
			model = GPy.models.GPRegression(X_scaled, Z, k)
			
			model.optimize()
			print('*' * 80)
			print('Optimized model:')
			print(model)
			print('Lengthscales:')
			print(model.kern.lengthscale)
			predict = lambda Xp: model.predict_noiseless((Xp - X_m) / X_s)
			#predict = lambda Xp: model.predict((Xp - X_m) / X_s)

			#Xt = np.linspace(40, 22.22, 200)[:,None]
			Xt = np.linspace(40, 20, 201)[:,None]

			#####################
			### Younger group ###
			#####################
			mean = np.array([5.866967, 73.18767, 73.21727]) # younger group, mean of mu, k1, k2 in GOH model
			
			std = mean*0.2/1.96 # 1.2 of mean becomes 95% confidence probability
			### Normal distribution
			p1 = mean[0] + std[0] * np.random.randn(1000)
			p2 = mean[1] + std[1] * np.random.randn(1000)
			p3 = mean[2] + std[2] * np.random.randn(1000)

			### Uniform distribution
			pp1 = np.random.uniform(mean[0]-3.*std[0],mean[0]+3.*std[0],1000)
			pp2 = np.random.uniform(mean[1]-3.*std[1],mean[1]+3.*std[1],1000)
			pp3 = np.random.uniform(mean[2]-3.*std[2],mean[2]+3.*std[2],1000)

			### For normal distribution
			
			tmp = np.zeros(((201,100000)))# temporary storage for each PC score
			for j in range(1000):
				Xp = np.hstack([np.matlib.repmat(p1[j], Xt.shape[0], 1),np.matlib.repmat(p2[j], Xt.shape[0], 1), np.matlib.repmat(p3[j], Xt.shape[0], 1), Xt])
				posteriorTestY = model.posterior_samples_f((Xp - X_m) / X_s, full_cov=True, size=100)
				#posteriorTestY = np.reshape(posteriorTestY,posteriorTestY.shape[0])
				tmp[:,j*100:(j+1)*100] = posteriorTestY
			
			
			pd1 = np.percentile(tmp,2.5,axis=1)
			pd2 = np.percentile(tmp,50,axis=1)
			pd3 = np.percentile(tmp,97.5,axis=1)

			np.savetxt('%iscore_%sFlap_ND_900T_y.txt'%(i+1,self.flap),np.array([pd1,pd2,pd3]).T,fmt='%.6f')
			
			
			### For uniform distribution

			tmp = np.zeros(((201,100000)))# temporary storage for each PC score
			for j in range(1000):
				Xp = np.hstack([np.matlib.repmat(pp1[j], Xt.shape[0], 1),np.matlib.repmat(pp2[j], Xt.shape[0], 1), np.matlib.repmat(pp3[j], Xt.shape[0], 1), Xt])
				posteriorTestY = model.posterior_samples_f((Xp - X_m) / X_s, full_cov=True, size=100)
				tmp[:,j*100:(j+1)*100] = posteriorTestY

			ppd1 = np.percentile(tmp,2.5,axis=1)
			ppd2 = np.percentile(tmp,50,axis=1)
			ppd3 = np.percentile(tmp,97.5,axis=1)
			np.savetxt('%iscore_%sFlap_UD_900T_y.txt'%(i+1,self.flap),np.array([ppd1,ppd2,ppd3]).T,fmt='%.6f')
			
			#######################
			# Without uncertainty #
			#######################

			Xp = np.hstack([np.matlib.repmat(mean[0], Xt.shape[0], 1),np.matlib.repmat(mean[1], Xt.shape[0], 1), np.matlib.repmat(mean[2], Xt.shape[0], 1), Xt])
			Mp, Vp = predict(Xp)
			Sp = np.sqrt(Vp)
			Lp = Mp - 1.96 * Sp
			Up = Mp + 1.96 * Sp

			ZZ = np.zeros((Mp.shape[0],3),dtype=float)
			ZZ[:,0] = np.reshape(Mp,Mp.shape[0])
			ZZ[:,1] = np.reshape(Up,Up.shape[0])
			ZZ[:,2] = np.reshape(Lp,Lp.shape[0])
			np.savetxt('%iscore_%sFlap_900T_noisefree_y.txt'%(i+1,self.flap),ZZ,fmt='%.6f')

			fig, ax = plt.subplots()
			plt.plot(Xt,Mp,color='red',label='Predictive mean')
			ax.fill_between(Xt.flatten(), ppd1.flatten(), ppd3.flatten(), alpha=0.25, label='95% predictive intervals (uniform distribution)')
			ax.fill_between(Xt.flatten(), pd1.flatten(), pd3.flatten(), alpha=0.25, label='95% predictive intervals (normal distribution)')
			ax.fill_between(Xt.flatten(), Lp.flatten(), Up.flatten(), alpha=0.25, label='95% predictive intervals (No uncertainty)')

			ax.set_ylabel(r'$\mathit{y_{%i}}$'%(i+1),fontname = "Arial",fontsize=26)
			plt.xticks(fontsize=23,fontname = "Arial")
			plt.yticks(fontsize=23,fontname = "Arial")
			plt.xlim([21,41])
			plt.xticks([21,26,31,36,41])
			ax.tick_params(axis='x', pad=15)
			ax.tick_params(axis='y', pad=15)
			x_axis = np.max(ax.get_xlim())-np.min(ax.get_xlim())
			y_axis = np.max(ax.get_ylim())-np.min(ax.get_ylim())
			ax.set_aspect(x_axis/y_axis)
			plt.tight_layout()
			plt.gcf().subplots_adjust(bottom=0.18,left=0.18)
			plt.legend(loc='best')
			#plt.show()
			plt.savefig('%iscore_%sFlap_UncertaintyPropa_y.pdf'%(i+1,self.flap), dpi=1200)
			

			###################
			### Elder group ###
			###################
			mean = np.array([70.81967, 8178.59, 80.27857]) # elder group
		
			std = mean*0.2/1.96 # 1.2 of mean becomes 95% confidence probability

			### Normal distribution
			p1 = mean[0] + std[0] * np.random.randn(1000)
			p2 = mean[1] + std[1] * np.random.randn(1000)
			p3 = mean[2] + std[2] * np.random.randn(1000)

			### Uniform distribution
			pp1 = np.random.uniform(mean[0]-3.*std[0],mean[0]+3.*std[0],1000)
			pp2 = np.random.uniform(mean[1]-3.*std[1],mean[1]+3.*std[1],1000)
			pp3 = np.random.uniform(mean[2]-3.*std[2],mean[2]+3.*std[2],1000)

			### For normal distribution
			
			tmp = np.zeros(((201,100000)))# temporary storage for each PC score
			for j in range(1000):
				Xp = np.hstack([np.matlib.repmat(p1[j], Xt.shape[0], 1),np.matlib.repmat(p2[j], Xt.shape[0], 1), np.matlib.repmat(p3[j], Xt.shape[0], 1), Xt])
				posteriorTestY = model.posterior_samples_f((Xp - X_m) / X_s, full_cov=True, size=100)
				tmp[:,j*100:(j+1)*100] = posteriorTestY
						
			pd1 = np.percentile(tmp,2.5,axis=1)
			pd2 = np.percentile(tmp,50,axis=1)
			pd3 = np.percentile(tmp,97.5,axis=1)
			np.savetxt('%iscore_%sFlap_ND_900T_o.txt'%(i+1,self.flap),np.array([pd1,pd2,pd3]).T,fmt='%.6f')
			
			### For uniform distribution
			
			tmp = np.zeros(((201,100000)))# temporary storage for each PC score
			for j in range(1000):
				Xp = np.hstack([np.matlib.repmat(pp1[j], Xt.shape[0], 1),np.matlib.repmat(pp2[j], Xt.shape[0], 1), np.matlib.repmat(pp3[j], Xt.shape[0], 1), Xt])
				posteriorTestY = model.posterior_samples_f((Xp - X_m) / X_s, full_cov=True, size=100)
				tmp[:,j*100:(j+1)*100] = posteriorTestY
			
			ppd1 = np.percentile(tmp,2.5,axis=1)
			ppd2 = np.percentile(tmp,50,axis=1)
			ppd3 = np.percentile(tmp,97.5,axis=1)
			np.savetxt('%iscore_%sFlap_UD_900T_o.txt'%(i+1,self.flap),np.array([ppd1,ppd2,ppd3]).T,fmt='%.6f')

			#######################
			# Without uncertainty #
			#######################

			Xp = np.hstack([np.matlib.repmat(mean[0], Xt.shape[0], 1),np.matlib.repmat(mean[1], Xt.shape[0], 1), np.matlib.repmat(mean[2], Xt.shape[0], 1), Xt])
			Mp, Vp = predict(Xp)
			Sp = np.sqrt(Vp)
			Lp = Mp - 1.96 * Sp
			Up = Mp + 1.96 * Sp

			ZZ = np.zeros((Mp.shape[0],3),dtype=float)
			ZZ[:,0] = np.reshape(Mp,Mp.shape[0])
			ZZ[:,1] = np.reshape(Up,Up.shape[0])
			ZZ[:,2] = np.reshape(Lp,Lp.shape[0])
			np.savetxt('%iscore_%sFlap_900T_noisefree_o.txt'%(i+1,self.flap),ZZ,fmt='%.6f')

			fig, ax = plt.subplots()
			plt.plot(Xt,Mp,color='red',label='Predictive mean')
			ax.fill_between(Xt.flatten(), ppd1.flatten(), ppd3.flatten(), alpha=0.25, label='95% predictive intervals (uniform distribution)')
			ax.fill_between(Xt.flatten(), pd1.flatten(), pd3.flatten(), alpha=0.25, label='95% predictive intervals (normal distribution)')
			ax.fill_between(Xt.flatten(), Lp.flatten(), Up.flatten(), alpha=0.25, label='95% predictive intervals (No uncertainty)')

			ax.set_ylabel(r'$\mathit{y_{%i}}$'%(i+1),fontname = "Arial",fontsize=26)
			plt.xticks(fontsize=23,fontname = "Arial")
			plt.yticks(fontsize=23,fontname = "Arial")
			plt.xlim([21,41])
			plt.xticks([21,26,31,36,41])
			ax.tick_params(axis='x', pad=15)
			ax.tick_params(axis='y', pad=15)
			x_axis = np.max(ax.get_xlim())-np.min(ax.get_xlim())
			y_axis = np.max(ax.get_ylim())-np.min(ax.get_ylim())
			ax.set_aspect(x_axis/y_axis)
			plt.tight_layout()
			plt.gcf().subplots_adjust(bottom=0.18,left=0.18)
			plt.legend(loc='best')
			#plt.show()
			plt.savefig('%iscore_%sFlap_UncertaintyPropa_o.pdf'%(i+1,self.flap), dpi=1200)
			