"""
Generates design points on which you should evaluate the model.

Feel free to change stuff accordingly.

This requires installing pyDOE which you can do by:

    pip install pyDOE


The design we are going to use is called a Latin Hypercube Design.

"""


import pyDOE as pd
import numpy as np


# Specify how many simulations you can afford:
num_simulations = 900

# The purpose here is to cover the range of possible inputs as well
# as possible.
bounds = np.array([[3.819, 241.68],   # [left, right] bounds, mu [kPa]
                   [0.304, 29436], # k_1 [kPa]
                   [0.090, 194.234]]) # k_2
                     

# The number of inputs that you have:
num_inputs = bounds.shape[0]

# Now I sample designs in [0,1]^3 and I am going to scale them back to the
# original bounds
X_scaled = pd.lhs(3, num_simulations)

# Let's get to the original space
X = X_scaled * (bounds[:, 1] - bounds[:, 0]) + bounds[:, 0]

# Save to a text file
np.savetxt('input900traning_span20%.txt', X, fmt='%.3f')
# You must do all these simulations...
