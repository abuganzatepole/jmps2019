import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import *
import numpy.matlib
from numpy import linalg as LA
import random
import myPCA
import myGPR


#Select type of flap among Adv, Trans, and Rot.
flap = 'Adv'

#import output data set for training; stress is saved as unit of MPa
t_s = np.loadtxt('allstress_%sFlap_900T.txt'%flap)

#import output data set for validation; stress is saved as unit of MPa
v_s = np.loadtxt('allstress_%sFlap_300V.txt'%flap)

#import input data set for training
t_i = np.loadtxt('input900training.txt')

#import input data set for validation
v_i = np.loadtxt('input300validation.txt')


PCmodel = myPCA.myPCA(flap,t_s,v_s)
[PC,t_score,v_score]= PCmodel.doPCA()
GPmodel = myGPR.myGPR(flap,t_i,t_score,v_i)
GPmodel.GPR_w_error()
